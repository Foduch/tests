BROWSER=$1

source venv/bin/activate
rm -rf allure_results
cd src
python -m pytest specs/ui/ --driver=$BROWSER --alluredir=../allure_results
cd ..
allure generate --clean ./allure_results
