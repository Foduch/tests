BRANCH=$1

cd /home/gitlab-runner/projects/timing_root

git checkout $BRANCH
git pull

docker-compose down -v
docker-compose pull
docker-compose up -d