from .base import BaseApi


class RaceApi(BaseApi):
    uri  = 'race/'

    def get_list(self, *args, **kwargs):
        result = self.get(self.uri)
        return result['data'], result['status']

    def create(self, race_data, *args, **kwargs):
        result = self.post(self.uri, data=race_data)
        return result['data'], result['status']

    def get_by_id(self, race_id, *args, **kwargs):
        result = self.get(self.uri + race_id)
        return result['data'], result['status']

    def update(self, id, data, *args, **kwargs):
        data_with_id = data
        data_with_id['guid'] = id
        result = self.put('{0}{1}'.format(self.uri, id), data=data_with_id)
        print(result)
        return result['data'], result['status']
