from .base import BaseApi


class GroupApi(BaseApi):
    uri = 'race/'

    def __get_race_uri(self, race_id):
        return '{0}{1}/group/'.format(self.uri, race_id)

    def __get_race_group_uri(self, race_id, group_id):
        return '{0}{1}/group/{2}'.format(self.uri, race_id, group_id)

    def retrieve_list(self, race_id, *args, **kwargs):
        result = self.get(self.__get_race_uri(race_id))
        return result['data'], result['status']

    def retrieve(self, race_id, group_id, *args, **kwargs):
        result = self.get(self.__get_race_group_uri(race_id, group_id))
        return result['data'], result['status']

    def create(self, race_id, group_data, *args, **kwargs):
        result = self.post(self.__get_race_uri(race_id), data=group_data)
        return result['data'], result['status']
