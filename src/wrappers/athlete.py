from .base import BaseApi


class AthleteApi(BaseApi):
    uri = 'athlete/'

    def get_athletes(self):
        result = self.get(self.uri)
        return result['data'], result['status']

    def get_athlete(self, id):
        result = self.get('{0}{1}'.format(self.uri, id))
        return result['data'], result['status']

    def create_athlete(self, athlete_data):
        result = self.post(self.uri, data=athlete_data)
        return result['data'], result['status']

    def update_athlete(self, id, athlete_data):
        data_with_id = athlete_data
        data_with_id['guid'] = id
        result = self.put('{0}{1}'.format(self.uri, id), data=data_with_id)
        return result['data'], result['status']

    def delete_athlete(self, id):
        result = self.delete('{0}{1}'.format(self.uri, id))
        return result['data'], result['status']

    def get_athlete_id(self, athlete_data):
        result = self.get(
            '{0}?first_name={1}&last_name={2}'.format(self.uri, athlete_data['first_name'], athlete_data['last_name'])
        )
        return result['data'][0]['guid'], result['status']
