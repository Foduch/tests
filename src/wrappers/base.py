import requests
import json


class BaseApi():
    base_uri = 'http://localhost:8000/api/'

    def __dumps(self, data):
        json_data = json.dumps(data)
        return json_data

    def __loads(self, json_data):
        data = json.loads(json_data)
        return data

    def get(self, uri, headers=None):
        response = requests.get(self.base_uri + uri)
        return {'status': response.status_code, 'data': response.json()}

    def post(self, uri, headers=None, data=None):
        '''
        returns: dict {'status': str, 'data': dict}
        '''
        if data is None:
            raise Exception('No data for post')
        if data:
            json_data = self.__dumps(data)
            response = requests.post(
                self.base_uri + uri,
                data=json_data,
                headers={"content-type": "application/json"}
            )
        else:
            raise Exception('Empty data for post')
        return {'status': response.status_code, 'data': response.json()}

    def put(self, uri, headers=None, data=None):
        json_data = self.__dumps(data)
        response = requests.put(
            self.base_uri + uri,
            data=json_data,
            headers={"content-type": "application/json", "Accept": "application/json"}
        )
        return {'status': response.status_code, 'data': response.json()}

    def delete(self, uri, headers=None):
        response = requests.delete(self.base_uri + uri, headers=headers)
        return {'status': response.status_code, 'data': {}}
