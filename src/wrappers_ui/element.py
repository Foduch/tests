import time
import allure
from selenium.webdriver.common.keys import Keys


class Element:
    '''
        Обертка элемента.
    '''
    def __init__(self, driver):
        self.driver = driver

    @allure.step('{message}')
    def log(self, message):
        pass

    def find(self, xpath):
        self.log('Ищем элемент "{}"'.format(xpath))
        for i in range(100):
            try:
                element = self.driver.find_element_by_xpath(xpath)
                return element
            except:
                time.sleep(0.1)
        raise Exception('No such element {}'.format(xpath))

    def set_value(self, element, value):
        self.log('Заполняем значением "{0}"'.format(value))
        while (element.get_attribute('value')):
            element.send_keys(Keys.BACKSPACE)
        element.clear()
        element.send_keys(value)

    def check_value(self, element, value):
        self.log('Проверяем значение {0} в элементе'.format(value))
        element_value = element.get_attribute('value')
        assert element_value == value

    def click(self, element):
        self.log('Кликаем по элементу')
        element.click()

    def wait_for_not_displayed(self, element):
        self.log('Ожидаем исчезновение элемента')
        for i in range(100):
            time.sleep(0.1)
            try:
                if element.is_displayed():
                    pass
                else:
                    return
            except:
                return
        raise Exception('Element still displayed')

    def wait_for_displayed(self, element):
        self.log('Ожидаем отображение элемента')
        for i in range(100):
            try:
                if element.is_displayed():
                    return
                else:
                    time.sleep(0.1)
            except:
                time.sleep(0.1)
        raise Exception('Element still not displayed')
