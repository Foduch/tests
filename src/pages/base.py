import config
import allure


from wrappers.athlete import AthleteApi
from wrappers.race import RaceApi
from wrappers_ui import Element
from component import SelectComponent, SnackbarComponent


class BasePage:
    '''
        Базовый класс страницы.
    '''
    def __init__(self, driver):
        self.driver = driver
        self.element = Element(driver)
        self.select = SelectComponent(driver)
        self.snackbar = SnackbarComponent(driver)
        self.athlete_api = AthleteApi()
        self.race_api = RaceApi()

    def open(self, url):
        '''
            Открытие страницы по прямой ссылке.
        '''
        self.driver.get(config.base_url + url)

    @allure.step('{message}')
    def log(self, message):
        pass
