import time

from pages.base import BasePage


class RacePage(BasePage):
    '''
        Страница гонки.
    '''
    def __init__(self, driver):
        super(RacePage, self).__init__(driver)

    def open_new(self):
        super().open('/race/new')

    def open(self, race_name):
        races = self.race_api.get_list()[0]
        for race in races:
            if race_name == race['name']:
                id = race['guid']
                super().open('/race/{0}'.format(id))
        if not(id):
            raise Exception('No such race {0}'.format(race_data['name']))

    @property
    def name_field(self):
        return self.element.find('//input[@name="name"]')

    @property
    def description_field(self):
        return self.element.find('//textarea[@name="description"]')

    @property
    def type_field(self):
        return self.element.find('//input[@name="race_type"]')

    @property
    def date_field(self):
        return self.element.find('//input[@name="start_date_time"]')

    @property
    def region_field(self):
        return self.element.find('//input[@name="region"]')

    @property
    def city_field(self):
        return self.element.find('//input[@name="city"]')

    @property
    def save_button(self):
        return self.element.find('//form//button[@type="submit"]')

    @property
    def switch_mode_button(self):
        return self.element.find('//button[@color="primary"]')

    @property
    def participant_tab(self):
        return self.element.find('//div[normalize-space(text())="Участники"]')

    @property
    def info_tab(self):
        return self.element.find('//div[normalize-space(text())="Общие сведения"]')

    @property
    def categories_add_button(self):
        return self.element.find('//button[*[normalize-space(text())="Добавить категорию"]]')

    @property
    def category_name_field(self):
        return self.element.find('//mat-dialog-container//input[@name="name"]')

    @property
    def category_gender_select(self):
        return self.element.find('//mat-dialog-container//mat-select[@name="gender"]')

    @property
    def category_add_button(self):
        return self.element.find('//mat-dialog-container//button[@type="submit"]')

    @property
    def add_participants_button(self):
        return self.element.find('//button[*[normalize-space(text())="Добавить участников"]]')

    def get_participant_add_button(self, first_name, last_name):
        return self.element.find('//mat-card[div[normalize-space(text())="{0}"]][div[normalize-space(text())="{1}"]]//button'.format(first_name, last_name))

    @property
    def participant_category_select(self):
        return self.element.find('//mat-select[@name="group"]')

    @property
    def participant_select_category_button(self):
        return self.element.find('//app-racer-select-group//button[@type="submit"]')

    def get_participant_added_text(self, first_name, last_name):
        return self.element.find('//mat-card[div[normalize-space(text())="{0}"]][div[normalize-space(text())="{1}"]]//div[normalize-space(text())="Добавлен"]'.format(first_name, last_name))

    def get_give_number_button(self, first_name, last_name):
        return self.element.find('//mat-list-item[*//*[normalize-space(text())="{0}"]][*//*[normalize-space(text())="{1}"]]//button'.format(first_name, last_name))

    def get_participant_number(self, first_name, last_name, number):
        return self.element.find('//mat-list-item[*//*[normalize-space(text())="{0}"]][*//*[normalize-space(text())="{1}"]]//span[normalize-space(text())="{2}"]'.format(first_name, last_name, number))

    @property
    def number_field(self):
        return self.element.find('//app-assign//input[@name="number"]')

    @property
    def tag_field(self):
        return self.element.find('//app-assign//input[@name="tag"]')

    @property
    def save_number_button(self):
        return self.element.find('//app-assign//button[@type="submit"]')

    def __set_data(self, data):
        self.log('Заполняем данные')
        self.element.set_value(self.name_field, data['name'])
        if 'description' in data:
            self.element.set_value(self.description_field, data['description'])
        self.element.set_value(self.type_field, data['race_type'])
        self.element.set_value(self.date_field, data['start_date_time'])

        if 'region' in data:
            self.element.set_value(self.region_field, data['region'])
        if 'city' in data:
            self.element.set_value(self.city_field, data['city'])

    def edit(self, data):
        self.element.click(self.switch_mode_button)
        self.__set_data(data)
        self.element.click(self.save_button)
        self.snackbar.wait('Данные обновлены')

    def create_race(self, data):
        self.__set_data(data)
        self.element.click(self.save_button)
        self.snackbar.wait('Гонка успешно создана')

    def open_participant_tab(self):
        self.element.click(self.participant_tab)
        time.sleep(0.5)

    def add_category(self, data):
        self.element.click(self.categories_add_button)
        self.element.set_value(self.category_name_field, data['name'])
        if (data['gender'] == 'm'):
            gender = 'Мужской'
        else:
            gender = 'Женский'
        self.select.set_option(self.category_gender_select, gender)
        self.element.click(self.category_add_button)
        self.element.wait_for_not_displayed(self.category_add_button)
        self.snackbar.wait('Категория добавлена')

    def add_participants(self, participants):
        self.element.click(self.add_participants_button)
        time.sleep(1)
        for participant in participants:
            first_name = participant['athlete']['first_name']
            last_name = participant['athlete']['last_name']
            self.element.click(self.get_participant_add_button(first_name, last_name))
            time.sleep(0.5)
            self.select.set_option(self.participant_category_select, participant['group'])
            self.element.click(self.participant_select_category_button)
            self.element.wait_for_not_displayed(self.participant_select_category_button)
            self.element.wait_for_displayed(self.get_participant_added_text(first_name, last_name))
            self.snackbar.wait('Участник добавлен')
            self.snackbar.wait_for_not_displayed('Участник добавлен')

    def give_numbers_and_tags(self, participants):
        for participant in participants:
            first_name = participant['athlete']['first_name']
            last_name = participant['athlete']['last_name']
            self.element.click(self.get_give_number_button(first_name, last_name))
            self.element.set_value(self.number_field, participant['number'])
            if 'tag' in participant:
                self.element.set_value(self.tag_field, participant['tag'])
            self.element.click(self.save_number_button)
            self.element.wait_for_not_displayed(self.save_number_button)
            self.element.wait_for_displayed(self.get_participant_number(first_name, last_name, participant['number']))
