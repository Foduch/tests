from pages.base import BasePage


class AthleteEditPage(BasePage):
    '''
        Страница редактирования профиля спортсмена.
    '''
    def __init__(self, driver):
        super(AthleteEditPage, self).__init__(driver)

    def open_new(self):
        super().open('/athlete/new')

    def open(self, athlete_data):
        id = self.athlete_api.get_athlete_id(athlete_data)[0]
        super().open('/athlete/{0}'.format(id))

    @property
    def first_name_input(self):
        return self.element.find('//input[@name="first_name"]')

    @property
    def last_name_input(self):
        return self.element.find('//input[@name="last_name"]')

    @property
    def gender_select(self):
        return self.element.find('//form//mat-select[@name="gender"]')

    @property
    def birthday_input(self):
        return self.element.find('//input[@name="birthday"]')

    @property
    def country_input(self):
        return self.element.find('//input[@name="country"]')

    @property
    def region_input(self):
        return self.element.find('//input[@name="region"]')

    @property
    def city_input(self):
        return self.element.find('//input[@name="city"]')

    @property
    def save_button(self):
        return self.element.find('//form//button[@type="submit"]')

    @property
    def switch_mode_button(self):
        return self.element.find('//button[@color="primary"]')

    def __set_data(self, data):
        self.log('Заполняем данные')
        self.element.set_value(self.first_name_input, data['first_name'])
        self.element.set_value(self.last_name_input, data['last_name'])
        self.element.set_value(self.birthday_input, data['birthday'])
        if 'country' in data:
            self.element.set_value(self.country_input, data['country'])
        if 'region' in data:
            self.element.set_value(self.region_input, data['region'])
        if 'city' in data:
            self.element.set_value(self.city_input, data['city'])
        if (data['gender'] == 'm'):
            gender = 'Мужской'
        else:
            gender = 'Женский'
        self.select.set_option(self.gender_select, gender)

    def edit(self, data):
        self.element.click(self.switch_mode_button)
        self.__set_data(data)
        self.element.click(self.save_button)
        self.snackbar.wait('Данные обновлены')

    def create_profile(self, data):
        self.__set_data(data)
        self.element.click(self.save_button)
        self.snackbar.wait('Профиль успешно создан')

    # def check_name(self):
    #     self.element.check_value(self.name_input, 'Name')
