import selenium
import pytest
import allure
from allure_commons.types import AttachmentType

from pages import AthleteEditPage, RacePage
from fixtures.test_1 import *
from fixtures import chrome_options, firefox_options


def test_create_athletes(selenium, athletes_fixture):
    athlete_edit_page = AthleteEditPage(selenium)
    for athlete in athletes_fixture:
        athlete_edit_page.open_new()
        athlete_edit_page.create_profile(athlete)
    allure.attach(selenium.get_screenshot_as_png(), name='screenshot', attachment_type=AttachmentType.PNG)


def test_create_race(selenium, race_fixture):
    race_page = RacePage(selenium)
    race_page.open_new()
    race_page.create_race(race_fixture)
    allure.attach(selenium.get_screenshot_as_png(), name='screenshot', attachment_type=AttachmentType.PNG)


def test_add_groups(selenium, race_fixture, female_group_fixture, male_group_fixture):
    race_page = RacePage(selenium)
    race_page.open(race_fixture['name'])
    race_page.open_participant_tab()
    race_page.add_category(female_group_fixture)
    race_page.add_category(male_group_fixture)
    allure.attach(selenium.get_screenshot_as_png(), name='screenshot', attachment_type=AttachmentType.PNG)


def test_add_participants(selenium, race_fixture, participants_fixture):
    race_page = RacePage(selenium)
    race_page.open(race_fixture['name'])
    race_page.open_participant_tab()
    race_page.add_participants(participants_fixture)
    allure.attach(selenium.get_screenshot_as_png(), name='screenshot', attachment_type=AttachmentType.PNG)


def test_give_number_and_tags(selenium, race_fixture, participants_fixture):
    race_page = RacePage(selenium)
    race_page.open(race_fixture['name'])
    race_page.open_participant_tab()
    race_page.give_numbers_and_tags(participants_fixture)
    allure.attach(selenium.get_screenshot_as_png(), name='screenshot', attachment_type=AttachmentType.PNG)
