from fixtures.story_rks import *
import wrappers.group as group
import wrappers.race as race
from constant import STATUS_CREATED, STATUS_BAD_REQUEST, STATUS_OK, STATUS_DELETED


race_id = ''
group_id = ''


class TestRKS():
    api = group.GroupApi()
    race_api = race.RaceApi()

    def test_create_race(self, race_data, *args, **kwargs):
        global race_id
        response_data, status_code = self.race_api.create(race_data)
        race_id = response_data['guid']
        assert status_code == STATUS_CREATED

    def test_create_group(self, group_data, *args, **kwargs):
        global group_id
        group_data_with_race_id = group_data
        group_data_with_race_id['race'] = race_id
        response_data, status_code = self.api.create(race_id, group_data_with_race_id)
        group_id = response_data['guid']
        assert status_code == STATUS_CREATED
        for key in group_data.keys():
            assert response_data[key] == group_data[key]

    def test_retrieve_group(self, group_data, *args, **kwargs):
        response_data, status_code = self.api.retrieve(race_id, group_id)
        assert status_code == STATUS_OK
        for key in group_data.keys():
            assert response_data[key] == group_data[key]

    def test_retrieve_list(self, *args, **kwargs):
        response_data, status_code = self.api.retrieve_list(race_id)
        assert status_code == STATUS_OK
        assert len(response_data) > 0
