from fixtures.story_rds import *
import wrappers.athlete as athlete
from constant import STATUS_CREATED, STATUS_BAD_REQUEST, STATUS_OK, STATUS_DELETED


athlete_id = ''


class TestRDS():
    api = athlete.AthleteApi()

    def test_create_full_data(self, athlete_full_data, *args, **kwargs):
        global athlete_id
        response_data, status_code = self.api.create_athlete(athlete_full_data)
        assert status_code == STATUS_CREATED
        athlete_id = response_data['guid']
        for key in athlete_full_data.keys():
            assert athlete_full_data[key] == response_data[key]

    def test_retrieve_one(self, athlete_full_data, *args, **kwargs):
        response_data, status_code = self.api.get_athlete(athlete_id)
        assert status_code == STATUS_OK
        for key in athlete_full_data.keys():
            assert athlete_full_data[key] == response_data[key]

    def test_create_required_data(self, athlete_only_required_data, *args, **kwargs):
        response_data, status_code = self.api.create_athlete(athlete_only_required_data)
        assert status_code == STATUS_CREATED
        for key in athlete_only_required_data.keys():
            assert athlete_only_required_data[key] == response_data[key]

    def test_required_fields(self, athlete_only_name_data, *args, **kwargs):
        response_data, status_code = self.api.create_athlete(athlete_only_name_data)
        assert status_code == STATUS_BAD_REQUEST

    def test_update(self, athlete_updated_data, *args, **kwargs):
        response_data, status_code = self.api.update_athlete(athlete_id, athlete_updated_data)
        assert status_code == STATUS_OK
        for key in athlete_updated_data.keys():
            assert athlete_updated_data[key] == response_data[key]

    def test_retrieve_updated(self, athlete_updated_data, *args, **kwargs):
        response_data, status_code = self.api.get_athlete(athlete_id)
        assert status_code == STATUS_OK
        for key in athlete_updated_data.keys():
            assert athlete_updated_data[key] == response_data[key]

    def test_filter(self, athlete_updated_data, *args, **kwargs):
        response_data, status_code = self.api.get_athlete_id(athlete_updated_data)
        assert status_code == STATUS_OK
        assert response_data == athlete_id

    def test_delete(self, *args, **kwargs):
        count_before = len(self.api.get_athletes()[0])
        response_data, status_code = self.api.delete_athlete(athlete_id)
        count_after = len(self.api.get_athletes()[0])
        assert status_code == STATUS_DELETED
        assert count_before != count_after
