from fixtures.story_fso import *
import wrappers.race as race
from constant import STATUS_CREATED, STATUS_BAD_REQUEST, STATUS_OK, STATUS_DELETED


race_id = ''


class TestFSO():
    api = race.RaceApi()

    def test_create_race(self, race_data, *args, **kwargs):
        global race_id
        response_data, status_code = self.api.create(race_data)
        race_id = response_data['guid']
        assert status_code == STATUS_CREATED
        for key in race_data.keys():
            assert race_data[key] == response_data[key]

    def test_update_race(self, updated_race, *args, **kwargs):
        response_data, status_code = self.api.update(race_id, updated_race)
        assert status_code == STATUS_OK
        for key in updated_race.keys():
            assert updated_race[key] == response_data[key]

    def test_retrieve_list(self, *args, **kwargs):
        response_data, status_code = self.api.get_list()
        assert status_code == STATUS_OK
        assert len(response_data) > 0

    def test_retrieve(self, updated_race, *args, **kwargs):
        response_data, status_code = self.api.get_by_id(race_id)
        assert status_code == STATUS_OK
        for key in updated_race.keys():
            assert updated_race[key] == response_data[key]
