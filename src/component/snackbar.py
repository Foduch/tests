from .base import BaseComponent


class SnackbarComponent(BaseComponent):
    def __init__(self, driver):
        super(SnackbarComponent, self).__init__(driver)

    def wait(self, text):
        element = self.element.find('//simple-snack-bar//span[normalize-space(text())="{}"]'.format(text))

    def wait_for_not_displayed(self, text):
        element = self.element.find('//simple-snack-bar//span[normalize-space(text())="{}"]'.format(text))
        self.element.wait_for_not_displayed(element)
