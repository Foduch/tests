from .base import BaseComponent


class SelectComponent(BaseComponent):
    def __init__(self, driver):
        super(SelectComponent, self).__init__(driver)

    def set_option(self, element, option):
        self.element.click(element)
        option_element = self.element.find('//mat-option//span[normalize-space(text())="{0}"]'.format(option))
        self.element.click(option_element)
        self.element.wait_for_not_displayed(option_element)
