from wrappers_ui import Element


class BaseComponent:
    def __init__(self, driver):
        self.driver = driver
        self.element = Element(driver)
