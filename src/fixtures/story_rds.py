import pytest

import constant


@pytest.fixture()
def athlete_full_data():
    return {
        'first_name': 'Все',
        'last_name': 'Данные',
        'gender': constant.MALE,
        'birthday': '2000-01-01',
        'country': 'Страна РДС',
        'region': 'Регион РДС',
        'city': 'Город РДС'
    }


@pytest.fixture
def athlete_updated_data():
    return {
        'first_name': 'Новые',
        'last_name': 'Данные',
        'gender': constant.FEMALE,
        'birthday': '2001-02-02',
        'country': 'РДС страна',
        'region': 'РДС регион',
        'city': 'РДС регион'
    }


@pytest.fixture
def athlete_only_required_data():
    return {
        'first_name': 'Основные',
        'last_name': 'Данные',
        'gender': constant.FEMALE,
        'birthday': '2000-01-02',
    }


@pytest.fixture
def athlete_only_name_data():
    return {
        'first_name': 'Спортсмен'
    }
