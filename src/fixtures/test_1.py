import pytest

import constant


athletes = [
    {
        "first_name": "Тест1",
        "last_name": "Первый",
        "gender": constant.MALE,
        "birthday": "1999-01-15"
    },
    {
        "first_name": "Тест1",
        "last_name": "Второй",
        "gender": constant.MALE,
        "birthday": "1985-06-01"
    },
    {
        "first_name": "Тест1",
        "last_name": "Третий",
        "gender": constant.MALE,
        "birthday": "1995-08-15"
    },
    {
        "first_name": "Тест1",
        "last_name": "Первая",
        "gender": constant.FEMALE,
        "birthday": "2000-05-12"
    },
    {
        "first_name": "Тест1",
        "last_name": "Вторая",
        "gender": constant.FEMALE,
        "birthday": "1999-08-23"
    }
]

race = {
    'name': 'Тест 1 гонка',
    'start_date_time': '01.09.2019 10:00',
    'race_type': 'XCO',
    'description': 'Описание тестовой гонки кросс-кантри',
    'region': 'Чувашская республика',
    'city': 'Чебоксары'
}

male_group = {
    'name': 'Мужчины',
    'gender': constant.MALE,
    "start_time": '2019-09-01T10:00:00Z'
}

female_group = {
    'name': 'Женщины',
    'gender': constant.FEMALE,
    "start_time": '2019-09-01T10:00:00Z'
}

participants = [
    {
        'athlete': athletes[0],
        'number': 10,
        'tag': '1010',
        'group': male_group['name']
    },
    {
        'athlete': athletes[1],
        'number': 11,
        'tag': '1111',
        'group': male_group['name']
    },
    {
        'athlete': athletes[2],
        'number': 12,
        'tag': '1212',
        'group': male_group['name']
    },
    {
        'athlete': athletes[3],
        'number': 20,
        'tag': '2020',
        'group': female_group['name']
    },
    {
        'athlete': athletes[4],
        'number': 21,
        'tag': '2121',
        'group': female_group['name']
    }
]


@pytest.fixture()
def athletes_fixture():
    return athletes


@pytest.fixture()
def male_group_fixture():
    return male_group


@pytest.fixture()
def female_group_fixture():
    return female_group


@pytest.fixture()
def participants_fixture():
    return participants


@pytest.fixture()
def race_fixture():
    return race
