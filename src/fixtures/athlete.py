import pytest


@pytest.fixture()
def athletes_list():
    return [{
        "first_name": "Тестовый1",
        "last_name": "Гонец1",
        "gender": "m",
        "birthday": "1999-01-15"
    },
        {
        "first_name": "Тестовый2",
        "last_name": "Гонец2",
        "gender": "f",
        "birthday": "2000-05-12"
    },
        {
        "first_name": "Тестовый3",
        "last_name": "Гонец3",
        "gender": "m",
        "birthday": "1985-06-01"
    },
        {
        "first_name": "Тестовый4",
        "last_name": "Гонец4",
        "gender": "f",
        "birthday": "1999-08-23"
    },
        {
        "first_name": "Тестовый5",
        "last_name": "Гонец5",
        "gender": "m",
        "birthday": "1995-08-15"
    },
    ]


@pytest.fixture()
def athlete_data_for_update():
    return {
        "id": 1,
        "first_name": "Обновленные",
        "last_name": "Данные",
        "gender": "f",
        "birthday": "1997-06-13"
    }
