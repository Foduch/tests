import pytest


@pytest.fixture()
def race_list():
    return [{

    },
        {

    },
        {

    },
    ]


@pytest.fixture()
def race_data():
    return {
        'name': 'Тестовая гонка 1',
        'start_date_time': '2019-03-31T11:00:00Z',
        'race_type': 'XCO',
        'description': 'Описание тестовой гонки кросс-кантри',
        'region': 'Чувашская республика',
        'city': 'Чебоксары'
    }
