import pytest


import constant


@pytest.fixture()
def race_data():
    return {
        'name': 'Тестовая гонка',
        'start_date_time': '2019-06-01T10:00:00Z',
        'race_type': 'XCO',
        'description': 'Описание тестовой гонки',
        'region': 'Регион гонки',
        'city': 'Город гонки'
    }


@pytest.fixture()
def group_data():
    return {
        'name': 'Тестовая категория',
        'gender': constant.MALE,
        'age_min': 20,
        "age_max": 90,
        "laps_max": 5,
        "start_time": '2019-06-01T10:30:00Z'
    }


@pytest.fixture()
def updated_group():
    return {
        'name': 'Тестовая категория (обновленная)',
        'gender': constant.FEMALE,
        'age_min': 16,
        "age_max": 60,
        "laps_max": 4,
        "start_time": '2019-06-01T10:40:00Z'
    }
