import pytest


@pytest.fixture()
def race_data():
    return {
        'name': 'Тестовая гонка',
        'start_date_time': '2019-06-01T10:00:00Z',
        'race_type': 'XCO',
        'description': 'Описание тестовой гонки',
        'region': 'Регион гонки',
        'city': 'Город гонки'
    }


@pytest.fixture()
def updated_race():
    return {
        'name': 'Обновленная тестовая гонка',
        'start_date_time': '2019-06-01T11:30:00Z',
        'race_type': 'XCO',
        'description': 'Обновленное описание тестовой гонки',
        'region': 'Регион гонки',
        'city': 'Новый регион гонки'
    }
