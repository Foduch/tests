import pytest


@pytest.fixture()
def groups_data():
    return [
        {
            'name': 'Мужчины Элита',
            'gender': 'm',
            'age_min': '23',
            'start_time': '2019-03-31T11:30:00Z'
        },
        {
            'name': 'Женщины Элита',
            'gender': 'f',
            'age_min': '23',
            'start_time': '2019-03-31T11:31:00Z'
        },
        {
            'name': 'Юниоры',
            'gender': 'm',
            'age_min': '17',
            'age_max': '18',
            'start_time': '2019-03-31T13:00:00Z'
        },
        {
            'name': 'Юниорки',
            'gender': 'f',
            'age_min': '19',
            'age_max': '18',
            'start_time': '2019-03-31T13:01:00Z'
        }
    ]
